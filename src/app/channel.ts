import { Link } from './link';

export class Channel {
    // tslint:disable-next-line:variable-name
    unique_name?: any;
    // tslint:disable-next-line:variable-name
    members_count: number;
    // tslint:disable-next-line:variable-name
    date_updated: Date;
    // tslint:disable-next-line:variable-name
    friendly_name: string;
    // tslint:disable-next-line:variable-name
    created_by: string;
    // tslint:disable-next-line:variable-name
    account_sid: string;
    url: string;
    // tslint:disable-next-line:variable-name
    date_created: Date;
    sid: string;
    attributes: string;
    // tslint:disable-next-line:variable-name
    service_sid: string;
    type: string;
    // tslint:disable-next-line:variable-name
    messages_count: number;
    links: Link[];
}
