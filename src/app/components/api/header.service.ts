import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  header: HttpHeaders;
  public url = 'https://chat.twilio.com/v2/Services/IS10217270b48042428d69d9bad325301b';

  constructor() {
    this.header = new HttpHeaders({Authorization: 'Basic ' + btoa('AC6d4fae90a594ca31a523ff0d68d4e7ab:b1fde75f14bf894cb7354ec48ea65bed')});
    this.header.append('Content-Type', 'application/x-www-form-urlencoded');
  }

  getHeader() {
    return {headers: this.header};
  }
}
