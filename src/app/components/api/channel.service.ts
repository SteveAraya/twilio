import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HeaderService } from './header.service';
import { Observable } from 'rxjs';
import { Channel } from 'src/app/channel';
import { ChatMessage } from '../../chatMessage';

const httpOptions = {
  headers: new HttpHeaders({
     'Content-Type': 'application/json',
     // tslint:disable-next-line:object-literal-key-quotes
     'Authorization': 'basic'
    })
};

@Injectable({
  providedIn: 'root'
})

export class ChannelService {

  private urlChanel: string;

  constructor(private http: HttpClient, private headerService: HeaderService) {
    // this.urlChanel = this.headerService.url + '/Channels';
  }

  // this method allows you to create channels.
  // tslint:disable-next-line:variable-name
  postChannel(friendly_name: string) {

    const body = new HttpParams()
    .set('FriendlyName', friendly_name);
    return this.http.post<Channel[]>(this.urlChanel, body, this.headerService.getHeader());

  }

  // this method allows a user to join a channel.
  joinChat(nickname: string, sid: string) {

    this.urlChanel = this.headerService.url + '/Channels';
    this.urlChanel += '/';
    this.urlChanel += sid;
    this.urlChanel += '/';
    this.urlChanel += 'Members';

    const body = new HttpParams()
    .set('Identity', nickname);
    return this.http.post(this.urlChanel, body, this.headerService.getHeader());
  }

  // this method allows to get all the messages.
  getMessages(sid: string): Observable<ChatMessage[]> {

    this.urlChanel = this.headerService.url + '/Channels';
    this.urlChanel += '/';
    this.urlChanel += sid;
    this.urlChanel += '/';
    this.urlChanel += 'Messages';

    return this.http.get<ChatMessage[]>(this.urlChanel, this.headerService.getHeader());

  }

  // this method allows you to send messages.
  sendMessage(sid: string, from: string, bodyM: string) {

    this.urlChanel = this.headerService.url + '/Channels';

    this.urlChanel += '/';
    this.urlChanel += sid;
    this.urlChanel += '/';
    this.urlChanel += 'Messages';

    const body = new HttpParams()
    .set('From', from)
    .set('Body', bodyM);
    return this.http.post(this.urlChanel, body, this.headerService.getHeader());

  }

  // this method allows you to edit the name of a channel
  // tslint:disable-next-line:variable-name
  editChannel(friendly_name: string, sid: string) {

    this.urlChanel = this.headerService.url + '/Channels';

    this.urlChanel += '/';
    this.urlChanel  += sid;

    const body = new HttpParams()
    .set('FriendlyName', friendly_name);
    // console.log(body.toString());
    return this.http.post<Channel>(this.urlChanel, body, this.headerService.getHeader());

  }

  // this method allows you to delete a channel.
  deleteChannel(sid: string) {

    this.urlChanel = this.headerService.url + '/Channels';

    this.urlChanel += '/';
    this.urlChanel  += sid;
    return this.http.delete<Channel[]>(this.urlChanel, this.headerService.getHeader());

  }

  // this method allows to get the channels.
  getChannels(): Observable<Channel[]> {

    this.urlChanel = this.headerService.url + '/Channels';

    return this.http.get<Channel[]>(this.urlChanel, this.headerService.getHeader());
  }

}
