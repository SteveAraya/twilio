import { Component, OnInit } from '@angular/core';
import { ChannelService } from '../api/channel.service';
import { NgForm } from '@angular/forms';
import swal from 'sweetalert';
import { Channel } from 'src/app/channel';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css']
})
export class MainViewComponent implements OnInit {

  public channels = [];
  public channel: Channel;

  constructor(private channelService: ChannelService, private router: Router) { }

  ngOnInit() {
    this.getChannels();
    // tslint:disable-next-line:new-parens
    this.channel = new Channel;

  }

  // this method allows to get the channels.
  getChannels() {

    this.channelService.getChannels().subscribe(data => {
      // tslint:disable-next-line:no-string-literal
      this.channels = data['channels'];

      console.log('Correct');
      }, err => {
      console.log('Error in service');
      });

  }

  // this method allows you to fill in the object's data in order to be able to match said data.
  loadData(channel: Channel) {
    this.channel = channel;
  }

  // this method allows a user to join a channel.
  join(form: NgForm) {

    if (form.value.user_id) {

      localStorage.setItem( 'nickname', JSON.stringify( form.value.user_id ) );
      localStorage.setItem( 'channel_sid', JSON.stringify( this.channel.sid ) );
      localStorage.setItem( 'channel_name', JSON.stringify( this.channel.friendly_name ) );

      this.channelService.joinChat(form.value.user_id, this.channel.sid).subscribe(data => {
        console.log('Correct');
        }, err => {
        console.log('Error in service');
        });

      this.router.navigate( ['/chat']);

    } else {
      swal({
        title: 'Error',
        text:  'Please, fill out the form',
        icon:  'error',
    });
    }
    form.reset({});
  }
}

