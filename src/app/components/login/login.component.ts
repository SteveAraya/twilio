import { Component, OnInit } from '@angular/core';

import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../../user';

import swal from 'sweetalert';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public list: User[]; // user list

  constructor(private router: Router) { }

  ngOnInit() {
  }

  // this method allows to authenticate a user to enter the administrator view
  login(form: NgForm) {

    this.list = JSON.parse ( localStorage.getItem( 'user' ) );

    for (const usu of this.list) {
      if (form.value.email === usu.email && form.value.password === usu.password) {
        localStorage.setItem( 'email', form.value.email);
        localStorage.setItem( 'name', usu.name);

        this.router.navigate( ['/adminView']);
      }
    }

    if (form.value.email === '' || form.value.password === '') {
      swal({
        title: 'Error',
        text:  'email or password incorrect, please try again',
        icon:  'error',
      });
    }
    console.log(localStorage.getItem( 'email'));

    if ( localStorage.getItem( 'email') === null) {
      swal({
        title: 'Error',
        text:  'email or password incorrect, please try again',
        icon:  'error',
      });
    }
  }
}
