import { Component, OnInit, ViewChild } from '@angular/core';
import { Channel } from 'src/app/channel';
import { ChannelService } from '../api/channel.service';
import { Router } from '@angular/router';
import swal from 'sweetalert';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-admin-view',
  templateUrl: './admin-view.component.html',
  styleUrls: ['./admin-view.component.css']
})
export class AdminViewComponent implements OnInit {

  public channels = []; // list channels
  public channel: Channel; // channel object

  constructor(private channelService: ChannelService, private router: Router) { }

  ngOnInit() {
    // tslint:disable-next-line:new-parens
    this.channel = new Channel;
    this.getChannels();
  }

  // this method allows you to fill in the object's data in order to be able to match said data.
  loadData(channel: Channel) {
    this.channel = channel;
  }

  // this method allows you to create new channels.
  createChannel(form: NgForm) {
    if (form.value.friendly_name) {
      this.channelService.postChannel(form.value.friendly_name).subscribe(data => {
        this.getChannels();
        form.reset({});
        console.log('Correct');
        }, err => {
        console.log('Error in service');
        });

    } else {
      swal({
        title: 'Missing data',
        text:  'Please, enter the Friendly Name',
        icon:  'warning',
    });
    }
  }

  // this method allows you to get all the channels.
  getChannels() {
    this.channelService.getChannels().subscribe(data => {
      // tslint:disable-next-line:no-string-literal
      this.channels = data['channels'];
      console.log('Correct');
      }, err => {
      console.log('Error in service');
      });
  }

  // this method allows you to edit the name of a channel.
  editChannel(form: NgForm) {
    if (form.value.friendly_name) {
      this.channelService.editChannel(form.value.friendly_name, this.channel.sid).subscribe(data => {
        this.getChannels();
        form.reset({});
        console.log('Correct');
        }, err => {
        console.log('Error in service');
        });

    } else {
      swal({
        title: 'Error',
        text:  'Please, try again',
        icon:  'error',
    });
    }
  }

  // this method allows you to delete a channel.
  deleteChannel(sid: string) {
    if (sid) {
      this.channelService.deleteChannel(sid)
        .subscribe(data => {
          this.getChannels();
        } );
    } else {
      swal({
        title: 'Error',
        text:  'Please, try again',
        icon:  'error',
    });
    }
  }

  // this method allows you to delete the 'email' key from localstorage.
  signOut() {
    localStorage.removeItem('email');
  }
}
