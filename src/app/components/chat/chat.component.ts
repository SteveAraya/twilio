import { Component, OnInit } from '@angular/core';
import { Member } from 'src/app/member';
import { ChatMessage } from 'src/app/chatMessage';
// import { ConsoleReporter } from 'jasmine';
import { ChannelService } from '../api/channel.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  // tslint:disable-next-line:no-inferrable-types
  public message: string = '';

  public messages = [];
  public chatMessage: ChatMessage;

  public nickname: string;
  public channelSID: string;
  // tslint:disable-next-line:variable-name
  public channel_name: string;

  constructor(private channelService: ChannelService) { }

  ngOnInit() {
    this.reloadMessages();
  }

  // this method allows you to load messages from a channel.
  loadMessages() {

    this.nickname = JSON.parse ( localStorage.getItem( 'nickname' ) );
    this.channelSID = JSON.parse ( localStorage.getItem( 'channel_sid' ) );
    this.channel_name = JSON.parse ( localStorage.getItem( 'channel_name' ) );

    this.channelService.getMessages(this.channelSID).subscribe(data => {
      // tslint:disable-next-line:no-string-literal
      this.messages = data['messages'];

      console.log('Correcto');
      }, err => {
      console.log('Error en servicio');
      });

  }

  // this method allows you to send a message to a channel.
  send_message() {

    if ( this.message.length === 0 ) {
      return;
    }

    this.channelService.sendMessage( this.channelSID, this.nickname, this.message ).subscribe(data => {

      this.message = '';
      console.log('Correcto');
      }, err => {
      console.log('Error en servicio');
      });

  }

  // this method allows you to make a call to the loadMessages method every second time.
  reloadMessages() {
    const time = timer(1000, 2000);
    time.subscribe(val => this.loadMessages());
  }

}
