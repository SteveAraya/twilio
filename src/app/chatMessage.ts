export class ChatMessage {
    body: string;
    index: number;
    // tslint:disable-next-line:variable-name
    channel_sid: string;
    from: string;
    // tslint:disable-next-line:variable-name
    date_updated: Date;
    type: string;
    // tslint:disable-next-line:variable-name
    account_sid: string;
    to: string;
    // tslint:disable-next-line:variable-name
    last_updated_by: string;
    // tslint:disable-next-line:variable-name
    date_created: Date;
    media: string;
    sid: string;
    url: string;
    attributes: string;
    // tslint:disable-next-line:variable-name
    service_sid: string;
    // tslint:disable-next-line:variable-name
    was_edited: string;
}
