export class Link {
    webhooks: string;
    messages: string;
    invites: string;
    members: string;
    // tslint:disable-next-line:variable-name
    last_message: any;
}
