import { Component } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Twilio';

  public user: User;
  public list: User[];

  constructor() {
    this.list = [];

    // Se inserta los usuarios para hacer las pruebas

    // Usuario numero 1
    this.user = new User('Alonso Rojas', 'alonso@gmail.com', '2222');
    this.list.push(this.user);

     // Usuario numero 2
    this.user = new User('Steve Araya', 'steve@gmail.com', '12345');
    this.list.push(this.user);

    // se limpia el objeto usuario
    this.user = new User('', '', '');

    // se inserta la lista usuario en localstorage
    localStorage.setItem( 'user', JSON.stringify( this.list ) );
  }
}
