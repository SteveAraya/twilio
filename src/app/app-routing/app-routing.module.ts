import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from '../components/login/login.component';
import { MainViewComponent } from '../components/main-view/main-view.component';
import { AdminViewComponent } from '../components/admin-view/admin-view.component';
import { ChatComponent } from '../components/chat/chat.component';


const routes: Routes = [
  { path: 'logIn', component: LoginComponent },
  { path: 'mainView', component: MainViewComponent },
  { path: 'adminView', component: AdminViewComponent },
  { path: 'chat', component: ChatComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'mainView' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
