export class Member {
    // tslint:disable-next-line:variable-name
    channel_sid: string;
    // tslint:disable-next-line:variable-name
    date_updated: Date;
    // tslint:disable-next-line:variable-name
    last_consumption_timestamp: string;
    // tslint:disable-next-line:variable-name
    account_sid: string;
    url: string;
    // tslint:disable-next-line:variable-name
    last_consumed_message_index: string;
    // tslint:disable-next-line:variable-name
    role_sid: string;
    sid: string;
    // tslint:disable-next-line:variable-name
    date_created: Date;
    // tslint:disable-next-line:variable-name
    service_sid: string;
    identity: string;
    attributes: string;
}
